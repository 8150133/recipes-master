package pt.ipp.estg.masterrecipes;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Utilizador.class}, version = 1, exportSchema = false)

public abstract class UtilizadorDB extends RoomDatabase {

    public abstract UtilizadorDAO getUserDao();

}
