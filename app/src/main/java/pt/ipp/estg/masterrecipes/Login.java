package pt.ipp.estg.masterrecipes;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    private Button login;
    private Button registo;
    private EditText email;
    private EditText password;
    private UtilizadorDB database;

    private UtilizadorDAO utilizadorDAO;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Check Utilizador...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);


        database = Room.databaseBuilder(this, UtilizadorDB.class, "mi-database.db")
                .allowMainThreadQueries()
                .build();

        utilizadorDAO = database.getUserDao();


        login = findViewById(R.id.login);
        registo = findViewById(R.id.registo);

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);



        registo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Registo.class));
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!emptyValidation()) {
                    progressDialog.show();

                    //handler permite enviar e processar mensagens e Runnable objects associados a threads das MessagesQueue
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utilizador utilizador = utilizadorDAO.getUser(email.getText().toString(), password.getText().toString());
                            if(utilizador !=null){
                                Intent i = new Intent(Login.this, Homepage.class);
                                i.putExtra("Utilizador", utilizador);
                                startActivity(i);
                                finish();
                            }else{
                                Toast.makeText(Login.this, "Utilizador inexistente, ou credenciais inválidas", Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();
                        }
                    }, 1000);

                }else{
                    Toast.makeText(Login.this, "Preencha todos os campos", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private boolean emptyValidation() {
        if (TextUtils.isEmpty(email.getText().toString()) || TextUtils.isEmpty(password.getText().toString())) {
            return true;
        }else {
            return false;
        }
    }
}
